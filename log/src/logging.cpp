#include "logging.h"
#include <map>
#include <vector>
#include <fstream>
#include <mutex>

namespace Logging
{
    typedef std::pair<std::string, int> TKey;
    typedef std::vector<TEnabler*> TPtrLevels;
    
    typedef std::pair<TPtrLevels, TEnabler> TValue;
    typedef std::map<TKey, TValue> TMap;

	std::atomic<bool>	m_sync_control;
	
	TMap& GetMap()
    {    
        static TMap m;
        return m;
    }

    std::mutex& GetMutex()
    {
		static std::mutex m;
        return m;
    }
    
    std::string ToString(TEnabler enabler)
    {
        if (enabler == ENABLED)
            return "ENABLED";
        if (enabler == DISABLED)
            return "DISABLED";
        if (enabler == DEFAULT)
            return "DEFAULT";
        if (enabler == NOT_INIT)
            return "NOT_INIT";
        throw std::logic_error("Invalid value");
        return "";
    }

    TEnabler Read(TEnabler& enabler)
    {
		// Get access to the variable and return its (up-to-date) value.

		atomic_thread_fence(std::memory_order_acquire);
		return reinterpret_cast<TEnabler&>(enabler);
    }

    void Write(TEnabler src, TEnabler& dest)
    {
		dest = src;
		atomic_thread_fence(std::memory_order_release);
		m_sync_control.store(true, std::memory_order_relaxed);
	}

    void Init(TEnabler& enabler, const char* pFile, int line)
    {
		std::unique_lock<std::mutex> lock(GetMutex(), std::try_to_lock);
		TKey key = std::make_pair(std::string(pFile), line);
        TMap::iterator i = GetMap().find(key);
 
        if (i != GetMap().end())
        {
            TValue& value = i->second;
            value.first.push_back(&enabler);
            Write(value.second, enabler);
            FILE_LOG(logDEBUG) << "Initialized the Logging: '" << 
                pFile << ":" << line << "' = '" << ToString(Read(enabler)) << "' (preset value)";
        }
        else
        {
            Write(DEFAULT, enabler);
            TPtrLevels levels;
            levels.push_back(&enabler);
            
            GetMap()[key] = std::make_pair(levels, DEFAULT);
            FILE_LOG(logDEBUG) << "Initialized the Logging for '" << 
                pFile << ":" << line << "' = '" << ToString(DEFAULT) << "'";
        }
    }

    void Set(const char* pFile, int line, TEnabler enabler)
    {
		std::unique_lock<std::mutex> lock(GetMutex(), std::try_to_lock);
        TKey key = std::make_pair(std::string(pFile), line);
        TMap::iterator i = GetMap().find(key);
 
        if (i != GetMap().end())
        {
            TValue& value = i->second;
            FILE_LOG(logDEBUG) << "Changing the value in Logging: '" << pFile << ":" << 
                line << "' = '" << ToString(enabler) << "' (old value = '" << ToString(value.second) << "')";
            value.second = enabler;
            TPtrLevels& levels = value.first;
            for (TPtrLevels::iterator j = levels.begin(); j != levels.end(); ++j)
            {
                Write(enabler, **j);
                FILE_LOG(logDEBUG) << "Set the Logging (initialized value): '" << 
                    pFile << ":" << line << "' = '" << ToString(enabler) << "'";
            }
        }
        else
        {
            GetMap()[key] = std::make_pair(TPtrLevels(), enabler);
            FILE_LOG(logDEBUG) << "Saved the value in Logging (add to the map): '" << pFile << ":" << 
                line << "' = '" << ToString(enabler) << "'";
       }
    }

    bool LogEnabled(TEnabler& enabler, const LogLevel& level, const char* pFile, int line)
    {
        TEnabler safeEnabler = Read(enabler);
        if (safeEnabler == NOT_INIT)
        {
            Init(enabler, pFile, line);    
            safeEnabler = Read(enabler);
        }
        return ((safeEnabler == DEFAULT && FILELog::ReportingLevel().Get() >= level) || safeEnabler == ENABLED);
    }
}
