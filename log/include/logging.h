#pragma once

#include "log.h"
#include "fenced.h"

namespace Logging
{
    enum TEnabler {NOT_INIT = -2, DEFAULT, DISABLED, ENABLED};
    void Init(TEnabler& enabler, const char* pFile, int line);
    void Set(const char* pFile, int line, TEnabler enabler);
    bool LogEnabled(TEnabler& enabler, const LogLevel& level, const char* pFile, int line);
}

#define LOG(level) \
    if (bool alreadyLogged = false); \
    else for (static Logging::TEnabler enabler = Logging::NOT_INIT; \
                 !alreadyLogged && Logging::LogEnabled(enabler, level, __FILE__, __LINE__); alreadyLogged = true) \
             FILELog().Set(__FILE__, __LINE__).Get(level)

/* -----------------------------------------------------------------------------
// Provide a function for formatting tracing messages that include the file name
//	and line number.
// ----------------------------------------------------------------------------- */
inline std::string	trace(std::string in_errMsg, const std::string in_filename, const int in_lineNumber) {
	std::ostringstream o;
	o << in_errMsg << "(" << in_filename << ":" << in_lineNumber << ")";
	return o.str();
}
inline std::string	trace(std::string in_errMsg, const std::string in_filename, const int in_lineNumber,
							int in_errorId) {
	std::ostringstream o;
	o << in_errMsg << "(result code=" << in_errorId << " at " << in_filename << ":" << in_lineNumber << ")";
	return o.str();
}
