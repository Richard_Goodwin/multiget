#pragma once

#include <atomic>

template <typename T>
struct Fenced
{
    Fenced(const Fenced<T>& f)
    {
        Set(f.t);
    }
    Fenced(const T& t)
    {
        Set(t);
    }
    Fenced& operator =(const Fenced<T>& f)
    {
        Set(f.t);
        return *this;
    }
    T Get()
    {
 		// Wait for any pending changes to the value being completed (and relaxing the lock).

		while (!m_sync_control.load(std::memory_order_relaxed))
			;

		// Get access to the variable and return its (up-to-date) value.

		atomic_thread_fence(std::memory_order_acquire);
        return reinterpret_cast<T&>(t);
    }
    void Set(T value)
    {
		// Use the fence to so the consumer of "t" gets the value only after it 
		// has been set in this function (i.e. that the value is up-to-date).

		t = value;
		atomic_thread_fence(std::memory_order_release);
		m_sync_control.store(true, std::memory_order_relaxed);
    }
private:
    T t;
	std::atomic<bool>	m_sync_control;
};


