Mutliget Example
----------------
Purpose:
An application that demonstrates one way of getting a large file from a server by fetching it in parts.

The program can be run as following:

multiget.exe [-o <output file name>] [-b <block size in bytes>] [-limit <max size>] URL
	-o If the <output file name> is not specified the file will be saved with the name at the end of the URL.
	-b If the <block size in bytes> is not specified the block size will be 1 Mb.
	-limit If <max size> is not specified only the first 4 Mb will be read.  If set to -1, the entire file will be read.


Folders:
--------
The folders in this Visual Studio C++ solution are:
.\log		A set of classes for logging execution 

.\lib		Static libraries for linked to the Multiget executable.

.\Multiget	A set of classes and code for getting a file in multiple parts from a remote location.

.\Debug\Multiget.exe		The debug version of the executable.
.\Run\Multiget.exe		The non-debug version of the executable.


Source:
=======
The code was written in C++ in Visual Studio 2013.

.\Multiget\src\
	main.cpp		The main entry point for the executable. 
	address			Represents the address of the server.from which the data is being requested.
	data_source		Represents the host connection.  Note, this implementation is using Windows sockets.
	connection		Provides a wrapper around the platform-specific API used to communicate with the server.
	message			Represents the full message received from the server.The message may contain multiple packets.
	message_block		Represents each block of data received from the server.
	request			Represents the request details that were sent to the server and conditions placed on that request like blocksize.
	byte_buffer		Manages the memory and contents of a buffer of raw bytes for passing data through API calls.
	http_buffer		A byte buffer that provides some operations over the data received from http get requests.