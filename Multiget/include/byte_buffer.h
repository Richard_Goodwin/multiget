#pragma once

#include <vector>
#include <algorithm>
#include <iterator>

/* ------------------------------------------------------------------------------
//	Provides a buffer containing generic byte storage so that it can be used 
//	with raw data APIs and manage the memory of the buffer dynamically.
------------------------------------------------------------------------------ */

#define KB(x)   ((size_t) (x) << 10)	// Express x number of kBytes
#define MB(x)   ((size_t) (x) << 20)	// Express x number of MBytes

typedef	char	byteType;

class ByteBuffer : public std::vector<byteType> {

public:

	static byteType	DEFAULT_BYTE_VALUE() { return 0x0; }

	ByteBuffer() {}
	ByteBuffer(std::size_t in_size, byteType in_value = DEFAULT_BYTE_VALUE());
	virtual ~ByteBuffer();

	ByteBuffer(const ByteBuffer& objRHS);
	ByteBuffer(const byteType objRHS[], std::size_t in_size);

	/* -----------------------------------------------------------------------------
	//	Support deep copy and assignment.
	// ----------------------------------------------------------------------------- */
	virtual	void	operator=(const ByteBuffer& objRHS);
	virtual	void	copyFrom(const byteType objRHS[], std::size_t in_size);
	virtual	void	copyFrom(const ByteBuffer& objRHS);
	virtual	void	copyFrom(const std::vector<byteType>::iterator& in_begin,
								const std::vector<byteType>::iterator& in_end);

	/* -----------------------------------------------------------------------------
	//	Add new contents to the end of this buffer.
	// ----------------------------------------------------------------------------- */
	void	append(const std::vector<byteType>& in_elements);

	/* -----------------------------------------------------------------------------
	//	Return a pointer to the start of the buffer.
	// ----------------------------------------------------------------------------- */
	template<class T>
	T*	getEmptyBufferAddress(std::size_t	in_size) {
		clear();
		resize(in_size, DEFAULT_BYTE_VALUE());
		return getEmptyBufferAddress<T>();
	}

	template<class T>
	T*	getEmptyBufferAddress() {
		return reinterpret_cast<T*>(data());
	}


	/* -----------------------------------------------------------------------------
	//	Return the position where the sequence of bytes starts in the contents of the
	//	buffer.
	// ----------------------------------------------------------------------------- */
	std::size_t	findPos(const std::vector<byteType>& in_search);
	std::vector<byteType>::iterator	find(const std::vector<byteType>& in_search);
	std::vector<byteType>::iterator	find(const std::vector<byteType>::iterator in_start,
											const std::vector<byteType>& in_search);

};