#pragma once

#include <string>
#include <algorithm>
#include <cctype>
#include <functional>
#include <windows.h>
#include "logging.h"
#include "connection.h"
#include "http_buffer.h"

/* -----------------------------------------------------------------------------
//	This class represents the dat and communication artifacts for reading that
//	data from its source.
// ----------------------------------------------------------------------------- */

class MessageBlock {

public:
	MessageBlock() 
		: size(0)
		, totalSize(0)
		, headerSize(0)
		, maxRangeByte(0) {}

	MessageBlock(MessageBlock& objFrom) { copyFrom(objFrom); }
	virtual ~MessageBlock() {
		clear();
	}

	/* -----------------------------------------------------------------------------
	//	Support deep copy and assignment.
	// ----------------------------------------------------------------------------- */
	virtual	void	operator=(MessageBlock& objRHS);
	virtual	void	copyFrom(MessageBlock& objRHS);

	/* -----------------------------------------------------------------------------
	//	Reset the MessageBlock to be empty.
	// ----------------------------------------------------------------------------- */
	void	clear();

	/* -----------------------------------------------------------------------------
	//	Add more bytes to the end of the data received so far.
	// ----------------------------------------------------------------------------- */
	void	append(ByteBuffer& in_MessageBlock);

	/* -----------------------------------------------------------------------------
	//	Read the data return through the socket.
	// ----------------------------------------------------------------------------- */
	void	receive(Connection& in_connection, Request& in_request);

	HttpBuffer		fullMessage;
	ByteBuffer		header;
	ByteBuffer		payload;

	long		size;
	long		totalSize;
	long		headerSize;
	long		payloadSize;
	long		maxRangeByte;

private:

	/* -----------------------------------------------------------------------------
	//	Get the size of the header from the response where the end of the header
	//	is marked by the empty line.
	// ----------------------------------------------------------------------------- */
	int separateParts();

};