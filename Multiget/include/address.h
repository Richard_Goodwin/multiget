#pragma once

#include <string>
#include <algorithm>
#include <cctype>
#include <functional>
#include "logging.h"

/* -----------------------------------------------------------------------------
//	The location of a data source or target with which a message can be communicated.
// ----------------------------------------------------------------------------- */

class Address {

public:

	Address() {}
	Address(const Address& objFrom) {
		copyFrom(objFrom);
	}
	virtual ~Address() {}

	static	const	std::string	DEFAULT_FILENAME;

	/* -----------------------------------------------------------------------------
	//	Support copying/assignment
	// ----------------------------------------------------------------------------- */
	virtual	Address&	operator=(const Address& objRHS);
	virtual	void	copyFrom(const Address& objRHS);

	std::string	getURL() const { return url; }
	std::string	getServer() const { return server; }
	std::string	getProtocol() const { return protocol; }
	std::string	getIPAddress() const { return ip_address; }
	int			getPort() const { return port; }
	std::string	getPath() const { return path; }
	std::string	getFilename() const { return filename; }
	std::string	getQueryParms() const { return queryparms; }

	void	setServer(std::string in_val)  { server = in_val; }
	void	setProtocol(std::string in_val) { protocol = in_val; }
	void	setIPAddress(std::string in_val) { ip_address = in_val; }
	void	setPort(int in_val) { port = in_val; }
	void	setPath(std::string in_val) { path = in_val; }
	void	setFilename(std::string in_val) { filename = in_val; }
	void	setQueryParms(std::string in_val) { queryparms = in_val; }

	/* -----------------------------------------------------------------------------
	//	Parse a URL into its components.
	// ----------------------------------------------------------------------------- */
	void	parseURL(const std::string& in_url);

private:

	std::string	url;
	std::string	ip_address;
	int			port;
	std::string protocol;
	std::string server;
	std::string path;
	std::string filename;
	std::string queryparms;

};

