#pragma once

#include <windows.h>
#include <string>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include "logging.h"
#include "address.h"
#include "message.h"
#include "request.h"
#include "connection.h"

/* ------------------------------------------------------------------------------
//	Represents the connection to a host through which requests will be sent 
//	and responses will be received.
------------------------------------------------------------------------------ */

class DataSource {

public:

	DataSource() {}
	virtual ~DataSource() {}

	/* -----------------------------------------------------------------------------
	//	Request data from an address and save the respond content to a local file.
	// ----------------------------------------------------------------------------- */
	Message*	submitRequest(Address& in_url, Request& in_request);

	/* -----------------------------------------------------------------------------
	//	Get the size of the header from the response where the end of the header
	//	is marked by the empty line.
	// ----------------------------------------------------------------------------- */
	Message*	read(Address&	in_addr, Request& in_request);

private:

	Connection		connection;

};