#pragma once

#include <sstream>
#include <string>
#include <stdio.h>
#include "fenced.h"
#include <mutex>

/* -----------------------------------------------------------------------------
//	Define a file stream protected to avoid simultaneous writes.
// ----------------------------------------------------------------------------- */

class ProtectedFile {

public:
	static Fenced<FILE*>& Stream();
	static void Output(const std::string& msg);

};

inline Fenced<FILE*>& FileStream()
{
	static Fenced<FILE*> pStream = 0;
	return pStream;
}

inline void FileStreamInit()
{
	FileStream() = stderr;
}

inline Fenced<FILE*>& ProtectedFile::Stream()
{
	static std::once_flag once;
	std::call_once(once, &FileStreamInit);
	return FileStream();
}

inline void ProtectedFile::Output(const std::string& msg)
{
	FILE* pStream = Stream().Get();
	if (!pStream)
		return;
	fprintf(pStream, "%s", msg.c_str());
	fflush(pStream);
}