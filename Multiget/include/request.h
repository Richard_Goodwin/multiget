#pragma once

#include <string>
#include <sstream>
#include "address.h"

/* -----------------------------------------------------------------------------
//	The full message communicated to or from a data source or a host.
// ----------------------------------------------------------------------------- */

class Request {

public:

	Request() :blockSize(0), requestBlock(0), limit(0) {}
	virtual ~Request() {}

	Request(const Request& objFrom);

	/* -----------------------------------------------------------------------------
	//	Support deep copy and assignment.
	// ----------------------------------------------------------------------------- */
	virtual	void	operator=(const Request& objRHS);
	virtual	void	copyFrom(const Request& objRHS);

	/* -----------------------------------------------------------------------------
	//	Return the HTTP request to be sent to the server.
	// ----------------------------------------------------------------------------- */
	const std::string		getRequestString(Address& in_addr);

	long	blockSize;		// Size in bytes of the data to read in one block.
	long	requestBlock;	// Note 0-based so the first block is block 0.
	long	limit;			// Maximum amount of data to read.
	long	startByte;		// The byte to start reading at.
	long	endByte;		// The last byte to read for this request.

};
