#pragma once

#include <windows.h>
#include <string>
#include <stdio.h>
#include <vector>

#include "logging.h"
#include "address.h"
#include "request.h"
#include "byte_buffer.h"
#include "http_buffer.h"

#pragma comment(lib,"ws2_32.lib")

/* -----------------------------------------------------------------------------
//	Provides connection-related operations so as to isolate the platform-specific
//	communication/socket API.  To use this class, use it in the following order:
//		1.	connect
//		2.	send (the request to the server)
//		3.	read (the response from the server for the request that was sent).
//		4.	close
// ----------------------------------------------------------------------------- */

class Connection {

public:

	enum connectionStateType { not_started, initialized, connected };
	
	Connection() 
		: state(not_started)
		, expectedBlockSize(0)
		, actualBlockSize(0)
	{}
	virtual ~Connection() {}

	void		connect(const std::string& serverName, WORD portNum);
	void		send(Address& in_addr, Request& in_request);
	HttpBuffer	read(long& totalBytesRead, Request& in_request);
	void		close();

private:

	void		initialize();

	SOCKET				connection;
	long				expectedBlockSize;
	long				actualBlockSize;
	connectionStateType	state;

};