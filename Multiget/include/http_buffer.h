#pragma once

#include <string>
#include <cctype>
#include <functional>
#include <algorithm>
#include <vector>
#include <iterator>

#include "byte_buffer.h"
#include "logging.h"

/* -----------------------------------------------------------------------------
//	This class extends the basic buffer of bytes to a buffer that is used for
//	http requests.  The primary extension is that the http buffer contains a
//	header block with communication information.
// ----------------------------------------------------------------------------- */

class HttpBuffer : public ByteBuffer {

public:

	HttpBuffer() 
		: expectedBlockSize(0)
		, actualBlockSize(0)
		, rangeStart(0)
		, rangeEnd(0)
		, totalBytes(0)
	{}
	HttpBuffer(std::size_t in_size, byteType in_value = DEFAULT_BYTE_VALUE()) 
		: ByteBuffer(in_size, in_value)		
	{ }
	virtual ~HttpBuffer() {
		ByteBuffer::~ByteBuffer();
	}

	HttpBuffer(const HttpBuffer& objRHS) { copyFrom(objRHS); }
	HttpBuffer(const byteType objRHS[], std::size_t in_size) { copyFrom(objRHS, in_size); }

	/* -----------------------------------------------------------------------------
	//	Support deep copy and assignment.
	// ----------------------------------------------------------------------------- */
	virtual	void	operator=(const HttpBuffer& objRHS) { 
		copyFrom(objRHS); 
	}
	virtual	void	copyFrom(const HttpBuffer& objRHS) {
		ByteBuffer::copyFrom(objRHS);
		expectedBlockSize = objRHS.expectedBlockSize;
		actualBlockSize = objRHS.actualBlockSize;
		rangeStart = objRHS.rangeStart;
		rangeEnd = objRHS.rangeEnd;
		totalBytes = objRHS.totalBytes;
	}
	virtual	void	copyFrom(const byteType objRHS[], std::size_t in_size) {
		ByteBuffer::copyFrom(objRHS, in_size);
		expectedBlockSize = 0;
		actualBlockSize = 0;
		rangeStart = 0;
		rangeEnd = 0;
		totalBytes = 0;
	}

	/* -----------------------------------------------------------------------------
	//	For the character parts of the buffer (e.g. headers), return the next 
	//	token of contiguous characters until a separator character.
	// ----------------------------------------------------------------------------- */
	std::string	getNextStringToken(std::vector<byteType>::iterator& in_iter);

	/* -----------------------------------------------------------------------------
	//	Return the number of bytes that are still expected (but have not been read)
	//	for this buffer.
	// ----------------------------------------------------------------------------- */
	long	getBytesRemaining() {
		return (expectedBlockSize - size());
	}
	
	/* -----------------------------------------------------------------------------
	//	Get the total size of the file from the response header.
	// ----------------------------------------------------------------------------- */
	void setExpectedBlockSize(long in_size) { expectedBlockSize = in_size; }
	void setActualBlockSize(long in_size) { actualBlockSize = in_size; }
	long getTotalTargetSize();
	long getActualBlockSize();
	long getExpectedBlockSize() const { return expectedBlockSize; }

	long	rangeStart;		// The position of the first byte of this message in the entire data.
	long	rangeEnd;		// The position of the last byte of this message in the entire data.
	long	totalBytes;		// The size of the entire data being read where this block is one part.

private:
	long	expectedBlockSize;	// The number of bytes that were requested for this message.
	long	actualBlockSize;	// The actual number of bytes returned in this one block of the entire data.

	
};
