#pragma once

#include <vector>
#include <algorithm>
#include <cctype>
#include <functional>
#include "address.h"
#include "message_block.h"
#include "byte_buffer.h"
#include <stdio.h>
#include <ostream>

/* -----------------------------------------------------------------------------
//	The full message communicated to or from a data source or a host.
// ----------------------------------------------------------------------------- */

class Message {

public:

	Message() : maxSize(0), totalSize(0), headerSize(0), contentLength(0) {}
	virtual ~Message();
	Message(const Message& objFrom);

	/* -----------------------------------------------------------------------------
	//	Support deep copy and assignment.
	// ----------------------------------------------------------------------------- */
	virtual	void	operator=(const Message& objRHS);
	virtual	void	copyFrom(const Message& objRHS);

	/* -----------------------------------------------------------------------------
	//	Accessors
	// ----------------------------------------------------------------------------- */
	void	setMaxMessageBlockSize(long in_maxSize) { maxSize = in_maxSize; }
	void	setTotalSize(long in_size) { totalSize = in_size; }
	void	captureMessageBlock(MessageBlock* in_newMessageBlock) {
		messageBlocks.push_back(in_newMessageBlock);
	}
	std::vector<MessageBlock*>	getMessageBlocks() const { return messageBlocks; }
	long	getTotalSize() const { return totalSize; }

	/* -----------------------------------------------------------------------------
	//	Send the binary contents of the message payload to the stream provided.
	// ----------------------------------------------------------------------------- */
	friend std::ostream& operator<<(std::ostream& os, const Message& in_msg);

private:

	/* -----------------------------------------------------------------------------
	//	Get the size of the header from the response where the end of the header
	//	is marked by the empty line.
	// ----------------------------------------------------------------------------- */
	int separateParts();
	
	long					maxSize;
	long					totalSize;
	std::vector<MessageBlock*>	messageBlocks;
	long					bytesRead;
	std::string				header;
	int						headerSize;
	long					contentLength;
	ByteBuffer				content;
};

