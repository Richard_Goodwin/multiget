#include "byte_buffer.h"

/* -----------------------------------------------------------------------------
//	Constructor/Destructor
// ----------------------------------------------------------------------------- */
ByteBuffer::ByteBuffer(std::size_t in_size, byteType in_value) {
	resize(in_size, in_value);
}

ByteBuffer::~ByteBuffer() {
	clear();
}

ByteBuffer::ByteBuffer(const ByteBuffer& objRHS) { 
	copyFrom(objRHS); 
}

ByteBuffer::ByteBuffer(const byteType objRHS[], std::size_t in_size) { 
	copyFrom(objRHS, in_size); 
}

/* -----------------------------------------------------------------------------
//	Support deep copy and assignment.
// ----------------------------------------------------------------------------- */
void	ByteBuffer::operator=(const ByteBuffer& objRHS) { 
	copyFrom(objRHS); 
}

void	ByteBuffer::copyFrom(const byteType objRHS[], std::size_t in_size) {
	this->clear();
	this->insert(end(), &objRHS[0], &objRHS[in_size]);
}

void	ByteBuffer::copyFrom(const ByteBuffer& objRHS) {
	this->clear();
	std::vector<byteType>::insert(end(), objRHS.begin(), objRHS.end());
}

void	ByteBuffer::copyFrom(const std::vector<byteType>::iterator& in_begin,
							const std::vector<byteType>::iterator& in_end) {
	this->clear();
	std::vector<byteType>::insert(end(), in_begin, in_end);
}

/* -----------------------------------------------------------------------------
//	Add new contents to the end of this buffer.
// ----------------------------------------------------------------------------- */
void	ByteBuffer::append(const std::vector<byteType>& in_elements) {
	std::copy(in_elements.begin(), in_elements.end(),
		std::back_inserter(*this));
}

/* -----------------------------------------------------------------------------
//	Return the position where the sequence of bytes starts in the contents of the
//	buffer.
// ----------------------------------------------------------------------------- */
std::size_t	ByteBuffer::findPos(const std::vector<byteType>& in_search) {
	auto it = std::search(begin(), end(), in_search.begin(), in_search.end());
	return std::distance(begin(), it);
}
std::vector<byteType>::iterator	ByteBuffer::find(const std::vector<byteType>& in_search) {
	return std::search(begin(), end(), in_search.begin(), in_search.end());
}
std::vector<byteType>::iterator	ByteBuffer::find(const std::vector<byteType>::iterator in_start,
	const std::vector<byteType>& in_search) {
	return std::search(in_start, end(), in_search.begin(), in_search.end());
}