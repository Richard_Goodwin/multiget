#include "http_buffer.h"

/* -----------------------------------------------------------------------------
//	Return the next series of characters that are not separators.
// ----------------------------------------------------------------------------- */
std::string	HttpBuffer::getNextStringToken(std::vector<byteType>::iterator& in_iter) {
	LOG(logDEBUG2) << "HttpBuffer::getNextStringToken - entry";

	static const byteType byteLinefeed = '\n';
	static const byteType byteReturn = '\r';
	static const byteType byteSpace = ' ';
	static const byteType byteHyphen = '-';
	static const byteType byteSlash = '/';

	std::string	result = "";
	byteType currByte;

	// Get the numbers in the content range.
	while (in_iter != end()) {

		currByte = *in_iter;
		in_iter++;

		switch (currByte) {
			// If the current byte is a hyphen or slash, include it in the 
			// accumulator but also treat it as a separator from the characters
			// that follow it.
			case byteHyphen:
			case byteSlash:
				if (result.empty()) {
					result += static_cast<char>(currByte);
					in_iter++;
				}
				// If the next byte is a separator return the 
				// string that has been accumulated so far if
				// there is one.
			case byteSpace:
			case byteReturn:
			case byteLinefeed:

				// If no string has been accumulated but a separator 
				// has been found, then skip over the separator and 
				// keep looking for characters to add.
				if (result.size() > 0) {
					--in_iter; // backup the iterator so the separator is 
								// processed as part of the next string.
					LOG(logDEBUG2) << "HttpBuffer::getNextStringToken - exit";
					return result;
				}
				break;

				// If the next byte is not a separator, add it
				// to the accumulator if it is a valid character.
				// Note that a hyphen and slash are a single-character tokens
				// so they get returned by itself.
			default:
				result += static_cast<char>(currByte);
				break;
		}
	}

	LOG(logDEBUG2) << "HttpBuffer::getNextStringToken - exit";
	return result;
}

/* -----------------------------------------------------------------------------
//	Parse the header received and determine how many bytes the server will provide.
// ----------------------------------------------------------------------------- */
long HttpBuffer::getActualBlockSize() 
{
	LOG(logDEBUG2) << "HttpBuffer::getActualBlockSize - entry";

	std::string		token;

	const ByteBuffer range_marker("Content-Range:", std::string("Content-Range:").size());
	const ByteBuffer transfer_marker("Tramsfer-encoding:", std::string("Tramsfer-encoding:").size());

	// Looking for Content-Range: bytes 0-1048575/402653352 where the final number is 
	// the total size.
	std::vector<byteType>::iterator rangeStats = find(range_marker);

	if (rangeStats != end()) {
		rangeStats += std::string("Content-Range:").size();

		token = getNextStringToken(rangeStats);
		rangeStart = std::stol(getNextStringToken(rangeStats));
		token = getNextStringToken(rangeStats);
		rangeEnd = std::stol(getNextStringToken(rangeStats));
		token = getNextStringToken(rangeStats);
		totalBytes = std::stol(getNextStringToken(rangeStats));

		// Calculate the actual size of the block.

		actualBlockSize = (rangeEnd - rangeStart + 1);
	}

	std::vector<byteType>::iterator transferStats = find(range_marker);

	LOG(logDEBUG2) << "HttpBuffer::getActualBlockSize - exit";
	return actualBlockSize;
}

/* -----------------------------------------------------------------------------
//	Get the size of the file being read from the response header.
// ----------------------------------------------------------------------------- */
long HttpBuffer::getTotalTargetSize() {

	// If the total bytes has not yet been determined, try to parse it from
	// the data if it already contains the header information.
	if (totalBytes != 0) {
		getActualBlockSize();
	}

	return totalBytes;
}