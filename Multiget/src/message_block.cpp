#include "message_block.h"

/* -----------------------------------------------------------------------------
//	Support deep copy and assignment.
// ----------------------------------------------------------------------------- */
void	MessageBlock::operator = (MessageBlock& objRHS) {
	return copyFrom(objRHS);
}
void	MessageBlock::copyFrom(MessageBlock& objRHS) {
	LOG(logDEBUG2) << "MessageBlock::copyFrom - entry";
	append(objRHS.fullMessage);
	LOG(logDEBUG2) << "MessageBlock::copyFrom - exit";
}

/* -----------------------------------------------------------------------------
//	Reset the block to be empty.
// ----------------------------------------------------------------------------- */
void	MessageBlock::clear() {
	LOG(logDEBUG2) << "MessageBlock::clear - entry";

	fullMessage.clear();
	header.clear();
	payload.clear();
	headerSize = 0;

	LOG(logDEBUG2) << "MessageBlock::clear - exit";
}

/* -----------------------------------------------------------------------------
//	Add more bytes to the end of the data received so far.
// ----------------------------------------------------------------------------- */
void	MessageBlock::append(ByteBuffer& in_MessageBlock) {
	LOG(logDEBUG2) << "MessageBlock::append - entry";

	fullMessage.append(in_MessageBlock);
	size += in_MessageBlock.size();

	LOG(logDEBUG2) << "MessageBlock::append - exit";
}

/* -----------------------------------------------------------------------------
//	Read the data return through the socket.
// ----------------------------------------------------------------------------- */
void	MessageBlock::receive(Connection& in_connection, Request& in_request) {
	LOG(logDEBUG2) << "MessageBlock::receive - entry";

	HttpBuffer	readBuffer;
	long		sizeRead = 0;

	// Get the next set of data from the connection.
	readBuffer = in_connection.read(sizeRead, in_request);

	// Copy the data received to the read buffer and look for the next response.
	append(readBuffer);
	separateParts();

	if (!fullMessage.empty()) {
		fullMessage.back() = 0x0;
		totalSize = fullMessage.size();
	}

	LOG(logDEBUG2) << "MessageBlock::receive - exit";
}

/* -----------------------------------------------------------------------------
//	Get the size of the header from the response where the end of the header
//	is marked by the empty line.
// ----------------------------------------------------------------------------- */
int MessageBlock::separateParts() {
	LOG(logDEBUG2) << "MessageBlock::separateParts - entry";

	char*		findPos = NULL;
	ByteBuffer	startHeader("HTTP", std::string("HTTP").size());
	ByteBuffer	endHeader1("\r\n\r\n", std::string("\r\n\r\n").size());
	ByteBuffer	endHeader2("\n\r\n\r", std::string("\n\r\n\r").size());

	// If the header has not yet been determined, find out
	// where it ends.

	if ((headerSize <= 0) && (!fullMessage.empty())) {

		headerSize = 0;

		// Copy the MessageBlock from the header until the header-end is found or
		// the end of the MessageBlock.

		ByteBuffer::iterator	headerStart = fullMessage.find(startHeader);
		ByteBuffer::iterator	headerEnd;

		if (headerStart != fullMessage.end()) {
			headerEnd = fullMessage.find(headerStart, endHeader1);

			if (headerEnd == fullMessage.end()) {
				headerEnd = fullMessage.find(headerStart, endHeader2);
			}
		}

		// If a header was found, separate the header from the payload.
		if ((headerStart != fullMessage.end()) && (headerEnd != fullMessage.end())) {

			// Make sure none of the payload was placed in front of the header.
			payload.insert(payload.end(), fullMessage.begin(), headerStart);

			// Copy the header aside and then copy the payload aside after 
			// moving forward past the end of header marker.
			header.copyFrom(headerStart, headerEnd);
			std::advance(headerEnd, endHeader1.size());
			payload.insert(payload.end(), headerEnd, fullMessage.end());

		} else {

			// If no header was found the entire block is payload.
			header.clear();
			payload.insert(payload.end(), fullMessage.begin(), fullMessage.end());
		}

		headerSize = header.size();
		payloadSize = payload.size();

	}

	LOG(logDEBUG2) << "MessageBlock::separateParts - exit";
	return headerSize;
}

