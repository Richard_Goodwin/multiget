#include "address.h"

const	std::string	Address::DEFAULT_FILENAME = "download.file";

/* -----------------------------------------------------------------------------
//	Assignment and deep copy
// ----------------------------------------------------------------------------- */
Address&	Address::operator=(const Address& objRHS) {
	copyFrom(objRHS);
	return *this;
}
void	Address::copyFrom(const Address& objRHS) {
	if (this != &objRHS) {
		url = objRHS.url;
		ip_address = objRHS.ip_address;
		port = objRHS.port;
		protocol = objRHS.protocol;
		server = objRHS.server;
		path = objRHS.path;
		//queryparms = objRHS.queryparms;
	}
}

/* -----------------------------------------------------------------------------
//	Parse a URL into its components.
// ----------------------------------------------------------------------------- */
void	Address::parseURL(const std::string& in_url) {
	LOG(logDEBUG2) << "Address::parseURL - entry (" << in_url << ")";

	const std::string prot_end("://");
	const std::string port_begin(":");

	url = in_url;

	// Get the protocol.
	std::string::const_iterator prot_i = search(in_url.begin(), in_url.end(), prot_end.begin(), prot_end.end());
	protocol.reserve(distance(in_url.begin(), prot_i));
	std::transform(in_url.begin(), prot_i, std::back_inserter(protocol), std::ptr_fun<int, int>(tolower));

	// If there is no more in the URL, stop because there is no host information.
	if (prot_i == in_url.end()) {
		LOG(logDEBUG1) << "Address::parseURL - no host information.";
		return;
	}

	// Get the host name/address.
	std::advance(prot_i, prot_end.length());
	std::string::const_iterator path_iter = find(prot_i, in_url.end(), '/');
	server.reserve(distance(prot_i, path_iter));
	std::transform(prot_i, path_iter, std::back_inserter(server), std::ptr_fun<int, int>(tolower));

	// Check the address for a port number.
	std::string::const_iterator port_iter = search(server.begin(), server.end(),
													port_begin.begin(), port_begin.end());

	if (port_iter != server.end()) {
		std::string	temp_port;
		temp_port.assign(port_iter + 1, server.end());
		port = std::stoi(temp_port);
		server = std::string(server.begin(), port_iter);
	} else {
		port = 80;
	}

	// Get the path information.
	std::string::const_iterator query_iter = find(path_iter, in_url.end(), '?');
	path.assign(path_iter, query_iter);

	// Get the final node as the name of the file.
	std::size_t		filenameStart = path.rfind("/");
	if (filenameStart != std::string::npos) {
		filename = path.substr(filenameStart + 1);
	} else {
		filename = DEFAULT_FILENAME;
	}

	// Get the querystring parameters.
	queryparms = "";
	if (query_iter != in_url.end()) {
		++query_iter;
		if (query_iter != in_url.end()) {
			queryparms.assign(query_iter, in_url.end());
		}
	}

	LOG(logDEBUG2) << "Address::parseURL - exit";
}