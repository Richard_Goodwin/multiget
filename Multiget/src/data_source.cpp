#include "data_source.h"

/* -----------------------------------------------------------------------------
//	Request data from an address and save the respond content to a local file.
// ----------------------------------------------------------------------------- */
Message*	DataSource::submitRequest(Address& in_url, Request& in_request) {
	LOG(logDEBUG2) << "DataSource::submitRequest - entry";

	Message*	result = NULL;
	result = read(in_url, in_request);

	LOG(logDEBUG2) << "DataSource::submitRequest - exit";
	return result;
}

/* -----------------------------------------------------------------------------
//	Get the size of the header from the response where the end of the header
//	is marked by the empty line.
// ----------------------------------------------------------------------------- */
Message*	DataSource::read(Address&	in_addr, Request& in_request) {
	LOG(logDEBUG2) << "DataSource::read - entry";

	Message*	response = new Message();
	Request		getRequest = in_request;
	bool		moreDataAvailble = true;
	long		dataReceived = 0;
	long		breaker = 0;

	// Use a loop in case the host closed the connection so getting
	// subsequent blocks requires a reconnection.

	while (moreDataAvailble && (breaker < 1000)) {
		breaker++;

		connection.connect(in_addr.getServer(), in_addr.getPort());

		while (true) {

			// Send the request contents to the server.
			connection.send(in_addr, getRequest);

			// Receive until the server closes the connection

			LOG(logDEBUG2) << "DataSource::read - reading MessageBlock " << getRequest.requestBlock
				<< " (size=" << getRequest.blockSize;

			MessageBlock*	MessageBlockReceived = new MessageBlock();
			MessageBlockReceived->receive(connection, getRequest);

			dataReceived += MessageBlockReceived->size;

			if (MessageBlockReceived->size > 0) {
				response->captureMessageBlock(MessageBlockReceived);
				response->setTotalSize(MessageBlockReceived->totalSize);

				// If some data was returned but the data received is less than a full block, exit due 
				// to end of data.
				if (MessageBlockReceived->size < getRequest.blockSize) {
					moreDataAvailble = false;
					break;
				}

				// If the data limit has been reached (or exceeded), stop.
				if ((getRequest.limit < dataReceived) && (getRequest.limit > 0)) {
					moreDataAvailble = false;
					break;
				}

			} else {
				// If the reason for no data returned was socket disconnection,
				// and there is more data to be read, reestablish the connection
				// for the next range of data.

				if ((dataReceived < response->getTotalSize()) && 
					((getRequest.limit <= 0) || (getRequest.limit > dataReceived))) {
					moreDataAvailble = true;
				} else {
					moreDataAvailble = false;
				}

				delete MessageBlockReceived;
				MessageBlockReceived = NULL;
				break;
			}

			getRequest.requestBlock = getRequest.requestBlock + 1;
		}

		connection.close();

	}
	LOG(logDEBUG2) << "DataSource::read - exit";
	return response;
}