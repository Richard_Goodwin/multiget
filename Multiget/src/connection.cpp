#include "connection.h"

/* -----------------------------------------------------------------------------
//	Request data from an address and save the respond content to a local file.
// ----------------------------------------------------------------------------- */
void	Connection::initialize() {
	LOG(logDEBUG2) << "Connection::submitRequest - entry";

	WSADATA wsaData;

	if (state == not_started) {
		if (WSAStartup(0x101, &wsaData) != NO_ERROR) {
			throw std::runtime_error(trace("Connection::initialize - Unable to connect to open a socket.", __FILE__, __LINE__));
		}

		state = initialized;
	}

	LOG(logDEBUG2) << "Connection::submitRequest - exit";
}

/* -----------------------------------------------------------------------------
//	Request data from an address and save the respond content to a local file.
// ----------------------------------------------------------------------------- */
void Connection::close() {
	LOG(logDEBUG2) << "Connection::close - entry";

	::WSACleanup();
	state = not_started;

	LOG(logDEBUG2) << "Connection::close - exit";
}

/* -----------------------------------------------------------------------------
//	Connect to the host and return the socket as the open connection.
// ----------------------------------------------------------------------------- */
void Connection::connect(const std::string& serverName, WORD portNum)
{
	LOG(logDEBUG2) << "Connection::connect - entry";

	struct hostent*		hp;
	unsigned int		addr;
	struct sockaddr_in server;

	LOG(logDEBUG1) << "Connection::connect - connecting to " << serverName;

	initialize();

	if (state == initialized) {
		connection = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (connection == INVALID_SOCKET) {
			throw std::runtime_error(trace("Connection::connect - invalid socket", __FILE__, __LINE__));
		}

		if (::inet_addr(serverName.c_str()) == INADDR_NONE) {
			hp = ::gethostbyname(serverName.c_str());
		} else	{
			addr = ::inet_addr(serverName.c_str());
			hp = ::gethostbyaddr((char*)&addr, sizeof(addr), AF_INET);
		}

		if (hp == NULL)	{
			::closesocket(connection);
			throw std::runtime_error(trace("Connection::connect - iunable to get the host.", __FILE__, __LINE__));
		}

		server.sin_addr.s_addr = *((unsigned long*)hp->h_addr);
		server.sin_family = AF_INET;
		server.sin_port = ::htons(portNum);

		int result = ::connect(connection, (struct sockaddr*)&server, sizeof(server));
		if (result == SOCKET_ERROR) {
			int	errorId = ::WSAGetLastError();
			::closesocket(connection);
			throw std::runtime_error(trace("Connection::connect - unable to establish a connection.", __FILE__, __LINE__, errorId));
		}

		state = connected;
	}

	LOG(logDEBUG2) << "Connection::connect - exit";
}

/* -----------------------------------------------------------------------------
//	Send a request to the server via the socket.
// ----------------------------------------------------------------------------- */
void	Connection::send(Address&	in_addr, Request& in_request) {
	LOG(logDEBUG2) << "Connection::send - entry";

	const std::string& temp_buffer = in_request.getRequestString(in_addr);
	LOG(logDEBUG1) << "Buffer being sent : \n" << temp_buffer;

	// If not already connected, try to connect.
	if (state != connected) {
		connect(in_addr.getServer(), in_addr.getPort());
	}

	// If it is connected, send the request.
	if (state == connected) {
		expectedBlockSize = in_request.blockSize;
		if (::send(connection, temp_buffer.c_str(), strlen(temp_buffer.c_str()), 0) == SOCKET_ERROR) {
			throw std::runtime_error(trace("Connection: unable to send through the socket.", __FILE__, __LINE__));
		}
	} else {
		throw std::runtime_error(trace("Connection: unable to send through the socket.", __FILE__, __LINE__));
	}

	LOG(logDEBUG2) << "Connection::send - exit";
}

/* -----------------------------------------------------------------------------
//	Read the data return through the socket.  The parameters to this routine
//	include the socket and the location of the buffer into which the data will
//	be copied.  Returns the number of bytes in that data buffer after reading.
//	0 (or less) means no data was read.
// ----------------------------------------------------------------------------- */
HttpBuffer	Connection::read(long& totalBytesRead, Request& in_request) {
	LOG(logDEBUG2) << "Connection::read - entry";

	const int	bufSize = KB(32);
	ByteBuffer	readBuffer(bufSize, ByteBuffer::DEFAULT_BYTE_VALUE());
	long		bytesRead = 0;
	long		readSize = bufSize;
	long		rangeEnd = 0;
	long		bytesRemaining = expectedBlockSize;
	HttpBuffer	dataBuffer;
	
	// If it is connected, read the response data through the socket.
	if (state == connected) {
		totalBytesRead = 0;
		actualBlockSize = 0;
		dataBuffer.setExpectedBlockSize(expectedBlockSize);

		while (true) {

			// Get the next set of data from the connection until it
			// returns no data.
			if (readBuffer.size() != bufSize) {
				readBuffer.resize(bufSize, ByteBuffer::DEFAULT_BYTE_VALUE());
			}
			std::fill(readBuffer.begin(), readBuffer.end(), ByteBuffer::DEFAULT_BYTE_VALUE());

			// Calculate the number of bytes available to receive in this read.
			if (bytesRemaining < bufSize) {
				readSize = bytesRemaining;
			} else {
				readSize = bufSize;
			}

			// Limit the data read if a maximum byte was provided.
			if ((in_request.limit > 0) && ((in_request.startByte + readSize) > in_request.endByte)) {
				readSize = in_request.endByte - in_request.startByte + 1;
				dataBuffer.setExpectedBlockSize(readSize);
			}

			// If there is nothing left to read, exit the loop.
			if (readSize <= 0) {
				break;
			}

			bytesRead = ::recv(connection, readBuffer.getEmptyBufferAddress<char>(), readSize, 0);
			if (bytesRead == 0) {
				break;
			}

			// Copy the data received to the read buffer and look for the next response.
			if (bytesRead != readSize) {
				readBuffer.resize(bytesRead);
			}
			dataBuffer.append(readBuffer);
			totalBytesRead += bytesRead;

			// If this is the first read, get the statistics from the header about the 
			// amount of data that is available.
			if (actualBlockSize == 0) {
				actualBlockSize = dataBuffer.getActualBlockSize();
				rangeEnd = dataBuffer.rangeEnd;
			}

			bytesRemaining = dataBuffer.getBytesRemaining();

		}
	} else {
		throw std::runtime_error(trace("Connection: unable to connect to the socket.", __FILE__, __LINE__));
	}

	LOG(logDEBUG2) << "Connection::read - exit";
	return dataBuffer;
}