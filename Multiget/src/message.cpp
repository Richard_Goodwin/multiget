#include "message.h"

/* -----------------------------------------------------------------------------
//	Release content maintained as a pointer to dynamic memory.
// ----------------------------------------------------------------------------- */
Message::~Message() {
	content.clear();
}

/* -----------------------------------------------------------------------------
//	Support deep copy and assignment.
// ----------------------------------------------------------------------------- */
Message::Message(const Message& objFrom) { copyFrom(objFrom); }

void	Message::operator=(const Message& objRHS) {
	return copyFrom(objRHS);
}
void	Message::copyFrom(const Message& objRHS) {
	for (MessageBlock* curr : messageBlocks) {
		delete curr;
	}
	messageBlocks.clear();

	for (MessageBlock* curr : objRHS.messageBlocks) {
		MessageBlock* newCopy = new MessageBlock(*curr);
		messageBlocks.push_back(newCopy);
	}
}

/* -----------------------------------------------------------------------------
//	Get the size of the header from the response where the end of the header
//	is marked by the empty line.
// ----------------------------------------------------------------------------- */
int Message::separateParts() {

	headerSize = 0;

	for (MessageBlock* curr : messageBlocks) {
		if (curr != NULL) {
			headerSize += curr->headerSize;
		}
	}

	return headerSize;
}

/* -----------------------------------------------------------------------------
//	Send the binary contents of the message payload to the output stream.
// ----------------------------------------------------------------------------- */
std::ostream&	operator<<(std::ostream& out_stream, const Message& in_msg) {
	LOG(logINFO) << "Message::operator<< - writing output to file " << std::endl;

	for (MessageBlock* curr : in_msg.getMessageBlocks()) {
		if (curr != NULL) {
			LOG(logINFO) << "Message::operator<< - writing " 
						<< curr->payload.size() << " bytes to file." << std::endl;
			out_stream.write(curr->payload.data(), curr->payload.size());
		}
	}

	return out_stream;
}
