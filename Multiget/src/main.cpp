#pragma once

#include "logging.h"
#include "data_source.h"
#include "address.h"
#include "message.h"
#include <windows.h>
#include <string>
#include <stdio.h>

#pragma comment(lib,"ws2_32.lib")

/* -----------------------------------------------------------------------------
// Main entry point from from a command line execution.
//	This program performs the following steps:
//		1. Accepts command line arguments
//		2. Sends a request to a server to get a file.
//		3. Receives the contents of that file as returned from the server through
//			socket connections.
//		4. Saves the contents received to a local file.
// ----------------------------------------------------------------------------- */

std::string	executableName = "";
std::string	outputFileName = "";
long		defaultBlockSize = MB(1);
long		limitSize = MB(4);
std::string	targetURL = std::string("http://28c5e526.bwtest-aws.pravala.com/384MB.jar");

/* -----------------------------------------------------------------------------
// Print out a short help message.
// ----------------------------------------------------------------------------- */
void	printHelp() {
	std::cout << "Usage: " << executableName
		<< " [-o <file>] [-b <size>] [-limit <bytes>] url" << std::endl
		<< "\t-o <file>\t Output the <file> instead of the default {" << outputFileName << "} " << std::endl
		<< "\t-b <size>\t Use <size> as the read block size instead of the default {" << defaultBlockSize << "} " << std::endl
		<< "\t-limit <bytes>\t Limit the data read to <bytes> instead of the default {" << limitSize << "} -1 is the full file." << std::endl
		<< "\turl\t The mandatory URL to the file being downloaded."
		<< std::endl;
}

enum argumentType {	tbd, exeName, outputFile, blockSize, limit, url , help, unknown } ;

/* -----------------------------------------------------------------------------
// Assign the argument value to the corresponding variable.
// ----------------------------------------------------------------------------- */
void	setParameterValue(argumentType	currArgument, std::string argument) {
	std::size_t		filenameStart;

	switch (currArgument) {
	case exeName: 
		executableName = argument;
		filenameStart = argument.rfind("/");
		if (filenameStart != std::string::npos) {
			executableName = argument.substr(filenameStart + 1);
		} else {
			executableName = argument;
		}
		break;
	case outputFile: 
		outputFileName = argument;
		break;
	case blockSize: 
		defaultBlockSize = std::stol(argument); 
		break;
	case limit:
		limitSize = std::stol(argument);
		break;
	case url:
		targetURL = argument; 
		break;
	case unknown:
	case help:
		printHelp();
	}
}

/* -----------------------------------------------------------------------------
// Get the command line arguments.  If an unknown parameter was passed or help was requested,
//	print out a short help message.
// ----------------------------------------------------------------------------- */
void	parseArguments(char* arguments[], int argument_count) {
	std::string		argument;
	argumentType	currArgument = tbd;

	setParameterValue(exeName, arguments[0]);

	// Ensure there are at least the required number of arguments.
	if (argument_count < 2) {
		printHelp();
		throw	std::invalid_argument("Missing mandatory argument.");
	} else {

		// Get the arguments and put them into the collection of settings.
		for (auto i = 0; i < argument_count; i++) {
			argument = arguments[i];

			// Determine which argument was provided.
			if (currArgument == tbd) {
				if (argument == "-o") {
					currArgument = outputFile;
				} else if (argument == "-b") {
					currArgument = blockSize;
				} else if (argument == "-limit") {
					currArgument = limit;
				} else if ((argument == "-h") || (argument == "-?") || (argument == "-help")) {
					printHelp();
					currArgument = tbd;
				} else if ((i == (argument_count - 1)) && (argument_count > 1)) {
					setParameterValue(url, argument);
					currArgument = tbd;
				} else if (i == 0) {
					setParameterValue(exeName, argument);
					currArgument = tbd;
				} else {
					currArgument = unknown;
				}
			} else {
				// Get the value for the argument if the argument type is already known.
				setParameterValue(currArgument, argument);
				currArgument = tbd;
			}
		}
	}
}

/* -----------------------------------------------------------------------------
// Main entry point from from a command line execution.
// ----------------------------------------------------------------------------- */
int	main(int argc, char* argv[]) {
	FILELog::ReportingLevel() = logINFO;
	LOG(logDEBUG2) << "Multiget - start";

	int		result = false;

	try {
		DataSource	host;
		Address		URL;
		Request		getRequest;

		// Get the arguments provided by the caller.
		parseArguments(argv, argc);

		// Send the request for data to the host and pull the response(s) back
		// as a message.
		getRequest.blockSize = defaultBlockSize;
		getRequest.limit = limitSize;

		URL.parseURL(targetURL);
		Message*		contentReceived = host.submitRequest(URL, getRequest);

		// For this test save the content to file.  First determine the name to use for the file.

		std::string	fileName;

		if (outputFileName.empty()) {
			fileName = URL.getFilename();	// The last node from the URL.
		} else {
			fileName = outputFileName;		// The file name provided as command-line option.
		}

		// Write the contents of the message received to the output file.
		std::ofstream outputFile(fileName, std::ios::out | std::ios::binary | std::ios::trunc);
		outputFile << (*contentReceived);
		outputFile.close();

		// Cleanup and shutdown.

		delete contentReceived;
		contentReceived = NULL;

	}
	catch (const std::exception& err) {
		LOG(logERROR) << err.what();
		result = -1;
	}

	return result;
}
