#include "request.h"

/* -----------------------------------------------------------------------------
//	Support deep copy and assignment.
// ----------------------------------------------------------------------------- */
Request::Request(const Request& objFrom) {
	copyFrom(objFrom);
}
void	Request::operator=(const Request& objRHS) {
	return copyFrom(objRHS);
}
void	Request::copyFrom(const Request& objRHS) {
	blockSize = objRHS.blockSize;
	requestBlock = objRHS.requestBlock;
	limit = objRHS.limit;
}

/* -----------------------------------------------------------------------------
//	Return the HTTP request to be sent to the server.
// ----------------------------------------------------------------------------- */
const std::string		Request::getRequestString(Address& in_addr) {
	const	std::string	endLine("\r\n");
	std::stringstream	sendBuffer;

	sendBuffer << "GET " << in_addr.getPath() << " HTTP/1.1" << endLine
		<< "Host:" << in_addr.getServer() << endLine;

	// If a block size is specified, request the specific range of bytes.

	if (blockSize > 0) {
		startByte = blockSize * requestBlock;
		endByte = startByte + blockSize - 1;

		// If there is a limit to the number of bytes to download, ensure the
		// requested end of the range does not exceed the limit.
		if (endByte > limit) {
			endByte = limit;
		}

		sendBuffer << "Range:bytes=" << startByte << "-" << endByte << endLine;
	}

	//sendBuffer << "User - Agent: HTTPGET 1.1" 
	sendBuffer << endLine;
	return sendBuffer.str();
}
